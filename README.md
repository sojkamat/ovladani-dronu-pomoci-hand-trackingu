# Ovládání dronu pomocí technik hand-trackingu
 
Tento projekt vznikl v rámci diplomové práce na FIT ČVUT. 
Naleznete zde jak výslednou spustitelnou aplikaci, tak i zdrojové soubory.

Videotutoriál zde: https://www.youtube.com/watch?v=wnsRnt5YiAE

- LEVEL_01: spustiletný soubor, 1. scéna s checkpointy
- LEVEL_02: spustiletný soubor, 2. scéna s checkpointy
- NEW ULTRALEAP: zdrojový Unity projekt
- TUTORIAL: spustiletný soubor, výuková scéna

